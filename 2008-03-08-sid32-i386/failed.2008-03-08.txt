9base 1:2-8 Failed 61.611582s UNKNOWN #441159: FTBFS in i386 chroot on amd64  RECHECK RECHECK_TIME(61/21)
adabrowse 4.0.2-5 Failed 21.772028s APT_GET_FAILED #445764: libasis-dev: unmet dep libgnatvsn-dev (>= 4.1)  RECHECK RECHECK_TIME(21/5)
adacontrol 1.6r8-1 Failed 21.161553s APT_GET_FAILED #445764: libasis-dev: unmet dep libgnatvsn-dev (>= 4.1)  RECHECK RECHECK_TIME(21/5)
aegis 4.22-2 Failed 5228.801479s UNKNOWN #461034: aegis: FTBFS: tests failed  RECHECK
alex 2.1.0~rc1-1 Failed 158.170098s NO_SUCH_FILE #460388: alex: FTBFS: Module `Distribution.Simple' does not export `compilerPath'  RECHECK RECHECK_TIME(158/64)
anki 0.9.5.2-1 Failed 70.71323s PYTHON_TRACEBACK #470334: anki: FTBFS: ImportError: No module named sqlalchemy
apt-howto 2.0.2-2 Failed 429.89548s NO_SUCH_FILE #432349: apt-howto: FTBFS: E: cannot find "apt-howto.de.sgml"  RECHECK RECHECK_TIME(429/313)
aptitude 0.4.10-1 Failed 553.691288s UNKNOWN #470054
apt-proxy 1.9.36.1 Failed 82.178697s UNKNOWN #470345: apt-proxy: FTBFS: po4a fails
aqsis 1.2.0-2 Failed 572.334069s DPKGSHLIB #439125: FTBFS: Cannot find debian/tmp/usr/lib/libaqsis.so.1.2  RECHECK RECHECK_GUESS RECHECK_TIME(572/41)
arch2darcs 1.0.12 Failed 80.926701s UNKNOWN GHC  RECHECK RECHECK_TIME(80/35)
arpack++ 2.2-10 Failed 21.298951s APT_GET_FAILED filed against libarpack2 RECHECK RECHECK_TIME(21/6)
asis 2005-5 Failed 19.697618s APT_GET_FAILED #445769: asis: FTBFS: unsat b-deps: gnat: Depends: gnat-4.1 but it is not going to be installed  RECHECK RECHECK_TIME(19/5)
baghira 0.8-1 Failed 310.592705s GCC_ERROR NNN
beagle 0.3.3-2 Failed 334.5912s COMPIL_FAILED #470328: beagle: FTBFS: GConfThreadHelper.cs(30,7): error CS0246: The type or namespace name `GLib' could not be found. Are you missing a using directive or an assembly reference?
beagle-xesam 0.1-1 Failed 257.793889s UNKNOWN #458716: beagle-xesam: FTBFS: checking for DBUS... configure: error: The pkg-config script could not be found or is too old.  RECHECK RECHECK_GUESS RECHECK_TIME(257/151)
bibletime 1.6.4.dfsg-1 Failed 238.475092s NO_SUCH_FILE #441161: bibletime - FTBFS: configure: error: Failed to compile the test program to check the Sword version  RECHECK RECHECK_TIME(238/75)
bigloo 2.8c-6 Failed 191.642701s NO_SUCH_FILE caused by sablevm #395260  RECHECK RECHECK_TIME(191/130)
bioperl 1.5.2.102-1 Failed 60.867637s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(60/19)
bisonc++ 2.4.1-1 Failed 193.906313s GCC_ERROR #465575: bisonc++: FTBFS: FlexLexer.h:130: error: expected unqualified-id before numeric constant  RECHECK RECHECK_TIME(193/129)
blam 1.8.4-3 Failed 258.127239s COMPIL_FAILED #470347: blam: FTBFS: Application.cs(13,1): error CS0246: The type or namespace name `Gnome' could not be found. Are you missing a using directive or an assembly reference?
bnfc 2.2-3 Failed 60.347069s UNKNOWN #460386: bnfc: FTBFS: lexical error in string/character literal at character '\t'  RECHECK RECHECK_TIME(60/30)
bogl 0.1.18-1.5 Failed 54.790826s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(54/16)
bristol 0.9.1-13 Failed 78.284404s NO_SUCH_FILE/GCC_ERROR #470336: bristol: FTBFS: midiSeqDevMan.c:280: error: 'SND_SEQ_EVENT_SAMPLE' undeclared here (not in a function)
bulmages 0.9.1-2 Failed 693.582813s LD_ERROR #456880: bulmages: FTBFS: dpkg-shlibdeps: failure: couldn't find library libbulmalib.so.1 needed by debian/bulmacont/usr/lib/bulmages/plugins/libregistroiva.so (its RPATH is '').  RECHECK RECHECK_TIME(693/526)
bzr-svn 0.4.7-1 Failed 81.189956s BDEPUNSAT sbuild problem with b-deps  RECHECK RECHECK_TIME(81/24)
cacao 0.98-2 Failed 208.457834s GCC_ERROR #458631: cacao: FTBFS: error: variable '_Jv_JNIInvokeInterface' has initializer but incomplete type  RECHECK RECHECK_TIME(208/92)
camelbones 0.2.3.cvs20040220-2.1 Failed 22.622616s APT_GET_FAILED #447373: camelbones: FTBFS: libgnustep-gui0.10-dev  RECHECK RECHECK_TIME(22/5)
cffi 20070901-2 Unknown 0s  same bug as the one against cl-asdf
chbg 1.5-9 Failed 22.401084s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(22/5)
cheops-ng 0.2.3-4.1 Failed 24.199451s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(24/5)
clanbomber 1.05cdbs-7 Failed 21.111671s APT_GET_FAILED filed against clanlib RECHECK RECHECK_TIME(21/6)
clanlib 0.6.5-1-4 Failed 320.379963s GCC_ERROR #467577: clanlib: FTBFS: Sources/Lua/clanbindings.cpp:43: error: 'tolua_tag' was not declared in this scope RECHECK
cl-pg 1:20061216-3 Failed 43.597332s UNKNOWN #470320: cl-pg: FTBFS: make: dh_lisp: Command not found
cl-plplot 0.4.0-2 Unknown 0s  same bug as the one against cl-asdf
cmucl 19d-20061116-4 Failed 207.660516s UNKNOWN #443705: FTBFS: sectsty.sty not found  RECHECK RECHECK_GUESS
codeine 1.0.1-3.dfsg-2 Failed 255.522012s GCC_ERROR jcristau
dag2html 1.01-2 Failed 59.002673s UNKNOWN #443029: dag2html: FTBFS: make[1]: camlp4r: Command not found  RECHECK RECHECK_TIME(59/17)
darcs 1.0.9-1 Failed 387.111726s UNKNOWN GHC  RECHECK RECHECK_TIME(387/190)
darcs-monitor 0.3.2-2 Failed 52.253304s UNKNOWN GHC  RECHECK RECHECK_TIME(52/18)
dballe 3.6-1 Failed 273.988065s UNKNOWN #453199: dballe: FTBFS: `Depends' field, syntax error after reference to package `libgcc1'  RECHECK RECHECK_TIME(273/158)
debian-edu 0.826 Failed 52.184076s NO_SUCH_FILE #470265: debian-edu: FTBFS: cp: cannot stat `./debian-edu-tasks.desc': No such file or directory
debian-edu-doc 1.0~20080215 Failed 336.01616s NO_SUCH_FILE #458879: debian-edu-doc: FTBFS: Invalid table entry row=1/column=1 (@colstart)  RECHECK RECHECK_TIME(336/219)
debian-med 0.16 Failed 49.335923s NO_SUCH_FILE #470271: debian-med: FTBFS: cp: cannot stat `./debian-med-tasks.desc': No such file or directory
debram 1.0.3 Failed 54.414201s GCC_ERROR #453160: debram: FTBFS: utf8.h:15: error: expected declaration specifiers or '...' before 'size_t'  RECHECK RECHECK_TIME(54/15)
denemo 0.7.7-2 Failed 174.812386s NO_SUCH_FILE #465628: denemo: FTBFS: libtool: link: `/usr/lib/libfftw3f.la' is not a valid libtool archive  RECHECK RECHECK_TIME(174/66)
dfu-util 0.0+r4067-3 Failed 53.875802s UNKNOWN #469892
dict-jargon 4.4.4-6.1 Failed 60.174009s UNKNOWN #470351: dict-jargon: FTBFS: patching fails
digitaldj 0.7.5-6 Failed 23.097187s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/5)
directory-administrator 1.7.1-1 Failed 23.375719s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/5)
drapes 0.5.1-3 Failed 261.719621s COMPIL_FAILED MONO
easymock 2.3+ds1-2 Failed 174.341564s UNKNOWN #470353: easymock: FTBFS: The import org.junit cannot be resolved
eclipse 3.2.2-5 Failed 891.40202s UNKNOWN #469758
eclipse-cdt 3.1.2-1 Failed 241.3946s UNKNOWN #432541: eclipse-cdt FTBFS  RECHECK RECHECK_TIME(241/103)
eclipse-pydev 1.2.5-2 Failed 196.560904s UNKNOWN #432539: eclipse-pydev: FTBFS: make: *** [build-java-stamp] Error 13  RECHECK RECHECK_TIME(196/84)
emile 0.11-2 Failed 23.52372s APT_GET_FAILED not for us  RECHECK RECHECK_TIME(23/6)
epplets 0.8.cvs.2005032801-3.2 Failed 20.890933s APT_GET_FAILED #436323: epplets: FTBFS: unmet b-dep libfnlib-dev (removed from unstable)  RECHECK RECHECK_TIME(20/5)
fdm 1.4-1 Failed 72.3981s GCC_ERROR #456871: fdm: FTBFS: /usr/include/tdb.h:150: error: expected ';', ',' or ')' before '*' token  RECHECK RECHECK_TIME(72/24)
freehdl 0.0.4-1 Failed 542.290655s UNKNOWN #443028: freehdl: FTBFS: make[3]: *** [vital_timing.lo] Error 1  RECHECK RECHECK_TIME(542/451)
ftm 0.0.8 Failed 62.358519s HEADER_NO_SUCH_FILE/NO_SUCH_FILE/GCC_ERROR #465576: ftm: FTBFS: clients.c:35:29: error: libiptc/libiptc.h: No such file or directory  RECHECK RECHECK_TIME(62/16)
gaim-galago 0.5.0-2 Failed 21.224786s APT_GET_FAILED #430949: gaim-galago needs porting to pidgin (untested patch included)  RECHECK RECHECK_TIME(21/5)
gbib 0.1.2-13 Failed 22.441078s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(22/6)
gcc-snapshot 20080224-1 Failed 365.157901s NO_SUCH_FILE/GCC_ERROR #470352: gcc-snapshot: FTBFS: checking for ssize_t... cc1: error: unrecognized command line option "-Wno-overlength-strings"
gcj-4.3 4.3-20080202-1 Failed 299.13176s UNKNOWN #467603: gcj-4.3: FTBFS: patching failed. RECHECK RECHECK_TIME(299/159)
gfax 0.7.6-7 Failed 204.411044s COMPIL_FAILED #470329: gfax: FTBFS: gui.cs(28,9): error CS0246: The type or namespace name `GConf.PropertyEditors' could not be found. Are you missing a using directive or an assembly reference?
gfslicer 1.5.4-7 Failed 23.227508s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/5)
ghc-cvs 20060905-1 Failed 287.269388s NO_SUCH_FILE #445752: ghc-cvs: FTBFS: multiple definition of `base_GHCziBase_zeze_closure'  RECHECK
gibraltar-bootcd 2.1 Failed 186.255314s DPKGSHLIB #453785: gibraltar-bootcd: FTBFS: dpkg-shlibdeps: failure: couldn't find library libc.so.0 needed by debian/mkinitrd-cd/usr/lib/mkinitrd-cd/paste (its RPATH is '').  RECHECK RECHECK_TIME(186/125)
glbsp 2.20-4 Failed 66.305251s UNKNOWN #460387: glbsp: FTBFS: dpkg-gencontrol: error: error occurred while parsing zlib1g , 'libglbsp2 (= 2.20-4) (= 2.20-4)', libc6 (>= 2.7-1),  RECHECK RECHECK_TIME(66/23)
glide 2002.04.10-16 Failed 325.461809s DPKGSHLIB #456875: glide: FTBFS: dpkg-shlibdeps: failure: couldn't find library libglide.so.2 needed by debian/glide2-bin/usr/lib/glide2/bin/test05 (its RPATH is '').  RECHECK
glotski 0.2-6 Failed 27.082454s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(27/5)
gmanedit 0.3.3-12.1 Failed 22.34911s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(22/6)
gnat-4.2 4.2.3-1 Failed 196.708793s NO_SUCH_FILE/GCC_ERROR #470335: gnat-4.2: FTBFS: cc1: error: unrecognized command line option "-Wno-overlength-strings"
gnat-4.3 4.3-20080202-1 Failed 132.310424s UNKNOWN #465671: gnat-4.3: sometimes builds gnattools before libgnatprj even though gnattools depends on libgnatprj
gnat-gdb 6.4+2006-2 Failed 105.808106s UNKNOWN #453209: gnat-gdb: FTBFS: make[4]: Nothing to be done for `info-am'.  RECHECK RECHECK_TIME(105/62)
gnat-glade 2006-3 Failed 21.572935s APT_GET_FAILED #445770: gnat-glade: FTBFS: unsat b-deps: gnat: Depends: gnat-4.1 but it is not going to be installed  RECHECK RECHECK_TIME(21/5)
gnat-gps 4.0.1-6 Failed 22.450078s APT_GET_FAILED #445772: gnat-gps: FTBFS: unsat b-deps: libgnatprj-dev: Depends: gnat-4.2 (= 4.2.1-7) but it is not going to be installed  RECHECK
gnome-rdp 0.2.2-5 Failed 211.640824s COMPIL_FAILED #470340: gnome-rdp: FTBFS: ./ProcessCaller.cs(75,27): error CS0246: The type or namespace name `Process' could not be found. Are you missing a using directive or an assembly reference?
gnudatalanguage 0.9~pre6-1 Failed 522.036255s GCC_ERROR #470244: gnudatalanguage: FTBFS: gdlgstream.cpp:41: error: invalid conversion from 'int (*)(char*)' to 'int (*)(const char*)'
gnustep-base 1.14.1-2 Failed 128.060773s UNKNOWN #449163: gnustep-base: FTBFS on arm: checking if ffcall trampolines work... no  RECHECK RECHECK_TIME(128/44)
gprolog 1.3.0-5 Failed 1185.942098s NO_SUCH_FILE/GCC_ERROR #456842: gprolog: FTBFS: /usr/include/asm-i386/sigcontext.h:19: error: redefinition of 'struct _fpreg'  RECHECK RECHECK_TIME(1185/760)
gst-editor 0.8.0-1 Failed 23.572248s APT_GET_FAILED #436324: gst-editor: FTBFS: unmet b-dep libgstreamer0.8-dev  RECHECK RECHECK_TIME(23/5)
gtkgo 0.0.10-15 Failed 22.855649s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(22/5)
gtkrsync 1.0.1 Failed 277.924842s NO_SUCH_FILE GHC  RECHECK RECHECK_TIME(277/115)
gtksourceview-sharp2 0.11-1 Failed 230.518405s COMPIL_FAILED #470324: gtksourceview-sharp2: FTBFS: ./PrintSample.cs(51,17): error CS0117: `GtkSourceView.SourcePrintJob' does not contain a definition for `upFromView'
gtwitter 1.0~beta-6 Failed 255.779083s COMPIL_FAILED #470350: gtwitter: FTBFS: ./PreferencesWindow.cs(112,31): error CS0234: The type or namespace name `Url' does not exist in the namespace `Gnome'. Are you missing an assembly reference?
guessnet 0.47-1 Failed 115.530647s GCC_ERROR #467580: guessnet: FTBFS: tut-main.cpp:12: error: 'runtime_error' is not a member of 'std' RECHECK
guile-gnome-platform 2.15.95-2 Failed 386.412899s UNKNOWN #454950 RECHECK RECHECK_TIME(386/241)
gwc 0.21.05-1 Failed 21.618114s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(21/7)
haddock 0.8-2 Failed 180.144278s NO_SUCH_FILE #460352: haddock: FTBFS: Could not find module `System.Directory':  RECHECK RECHECK_TIME(180/74)
happs 0.8.8+darcs20070523-2 Failed 82.530766s BDEPUNSAT #443053: FTBFS: multiple definitions  RECHECK RECHECK_TIME(82/27)
happy 1.16~rc2-1 Failed 154.081462s NO_SUCH_FILE #460375: happy: FTBFS: Module `Distribution.Simple' does not export `compilerPath'  RECHECK RECHECK_TIME(154/54)
haskell98-report 20030706-3 Failed 344.017832s UNKNOWN #420479: FTBFS: dvips: ! Couldn't find header file times.ttf.  RECHECK RECHECK_TIME(344/188)
haskell98-tutorial 200006-2-1 Failed 114.301225s UNKNOWN #419397: haskell98-tutorial: FTBFS after the tetex to texlive transition  RECHECK RECHECK_TIME(114/33)
haskell-cabal 1.1.3 Failed 82.666944s UNKNOWN #460354: haskell-cabal: FTBFS: Could not find module `Text.PrettyPrint':  RECHECK RECHECK_TIME(82/40)
hdbc-missingh 1.0.1.1 Failed 21.415797s APT_GET_FAILED #410835: FTBFS: Could not find module `MissingH.AnyDBM'  RECHECK RECHECK_TIME(21/5)
henplus 0.9.7.ds1-1 Failed 149.521523s BDEPUNSAT #458674: henplus: FTBFS: unmet b-dep libcommons-cli-java(inst 1.1-1 ! << wanted 1.1)  RECHECK RECHECK_TIME(149/46)
hk-classes 0.8.3-3 Failed 1221.946163s UNKNOWN #470338: hk-classes: FTBFS: dh_install: libhk-classes-xbase missing files (usr/lib/libhk_xbasedriver.so*), aborting
hs-plugins 0.9.10-3.4 Failed 348.04253s NO_SUCH_FILE #460374: hs-plugins: FTBFS: Constructor `STArray' should have 4 arguments, but has been given 3  RECHECK RECHECK_TIME(348/187)
ikvm 0.34.0.4-2 Failed 256.181548s UNKNOWN #458676: ikvm: FTBFS: error CS0006: cannot find metadata file `System.Windows.Forms.dll'  RECHECK RECHECK_TIME(256/131)
illuminator 0.10.0-4 Failed 564.932725s UNKNOWN #443033: illuminator: FTBFS: configure: error: "PETSc libraries not found"  RECHECK RECHECK_TIME(564/316)
imapsync 1.241-1 Failed 67.205254s UNKNOWN #464300: imapsync: FTBFS: Can't locate Mail/IMAPClient.pm in @INC  RECHECK RECHECK_TIME(67/22)
imms 3.0.2-2 Failed 22.928319s APT_GET_FAILED #456686: dependency xmms gone
inkscape 0.45.1-1 Failed 1207.364056s UNKNOWN #436333: inkscape: FTBFS: Global symbol "@INTLTOOL_ICONV" requires explicit package name at ./intltool-merge line 94.  RECHECK RECHECK_TIME(1207/907)
insight 6.7.1.dfsg.1-8 Failed 247.989985s UNKNOWN #464314: insight: FTBFS: checking for Itcl configuration... configure: error: /usr/lib/itcl3.2 directory doesn't contain itclConfig.sh  RECHECK
ivtools 1.1.3-5.3 Failed 186.363549s NO_SUCH_FILE/GCC_ERROR #441490: ivtools - FTBFS: stl_algobase.h:226:56: error: macro "min" passed 3 arguments, but takes just 2  RECHECK RECHECK_TIME(186/94)
jfreereport 0.9.0-05-4 Failed 214.616291s UNKNOWN #467601: jfreereport: FTBFS: The method getPlattformDefaultEncoding() is undefined for the type EncodingRegistry RECHECK RECHECK_TIME(214/89)
kaffe 2:1.1.8-3 Failed 1352.762344s NO_SUCH_FILE/GCC_ERROR #470341: kaffe: FTBFS: constants_check.h:73: error: 'SND_SEQ_EVENT_SAMPLE' undeclared (first use in this function)
kdenetwork 4:3.5.9-1 Failed 1345.346376s NO_SUCH_FILE/GCC_ERROR jcristau
keybled 0.65-7 Failed 235.003185s GCC_ERROR jcristau
kimwitu-doc 10a+1-2 Failed 117.412523s NO_SUCH_FILE #430794: kimwitu-doc - FTBFS: sh: gs: command not found  RECHECK RECHECK_TIME(117/39)
kino 1.2.0-1 Failed 24.438008s APT_GET_FAILED #462508: kino: FTBFS: /usr/bin/ld: cannot find -lGL  RECHECK RECHECK_TIME(24/8)
koffice 1:1.6.3-4 Failed 1372.499528s NO_SUCH_FILE/GCC_ERROR jcristau
kuake 0.3-5.2 Failed 241.86742s LD_ERROR/NO_SUCH_FILE #470243: kuake: FTBFS: kuake.cpp:(.text+0x8b): undefined reference to `KParts::MainWindow::qt_emit(int, QUObject*)'
labplot 1.6.0.1-1 Failed 702.027507s GCC_ERROR #470246: labplot: FTBFS: ImportOPJ.cc:47: error: no matching function for call to 'Spreadsheet::setColumnType(int&, ColumnType)'
lasso 2.1.1-2 Failed 563.853528s NO_SUCH_FILE #464319: lasso: FTBFS: mv: cannot stat `/build/user/lasso-2.1.1/debian/tmp/usr/lib/java/*.so': No such file or directory  RECHECK RECHECK_TIME(563/390)
libapache-mod-layout 3.2.1-2 Failed 21.715456s APT_GET_FAILED #429126: please update/request removal of your package  RECHECK RECHECK_TIME(21/5)
libapache-mod-random 1.4-10.1 Failed 21.206636s APT_GET_FAILED #429085: please update/request removal of your package  RECHECK RECHECK_TIME(21/6)
libapache-mod-tsunami 3.0-1.1 Failed 22.040672s APT_GET_FAILED #429133: please update/request removal of your package  RECHECK RECHECK_TIME(22/5)
libapp-cache-perl 0.33-1 Failed 73.879836s UNKNOWN #465642: libapp-cache-perl: FTBFS: Failed 1/3 test scripts, 66.67% okay. 3/37 subtests failed, 91.89% okay.  RECHECK RECHECK_TIME(73/37)
libaws 2.2dfsg-1 Failed 28.294918s APT_GET_FAILED #445764: libasis-dev: unmet dep libgnatvsn-dev (>= 4.1)  RECHECK RECHECK_TIME(28/7)
libbuffy 0.9-1 Failed 91.699131s GCC_ERROR #467574: libbuffy: FTBFS: tut-main.cpp:12: error: 'runtime_error' is not a member of 'std' RECHECK RECHECK_TIME(91/33)
libbusiness-onlinepayment-transactioncentral-perl 0.06-1 Failed 70.157008s UNKNOWN #470332: libbusiness-onlinepayment-transactioncentral-perl: FTBFS: Failed 5/13 test scripts, 61.54% okay. 5/13 subtests failed, 61.54% okay.
libcairo-ruby 1.5.0-1 Failed 112.535003s UNKNOWN #458679: libcairo-ruby: FTBFS: :0:in `require': no such file to load -- ftools (LoadError)  RECHECK RECHECK_TIME(112/38)
libclass-csv-perl 1.03-1 Failed 74.065534s NO_SUCH_FILE #462519: libclass-csv-perl: FTBFS: Base class package "Class::Accessor" is empty.  RECHECK
libclass-trait-perl 0.22-2 Failed 62.906985s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(62/22)
libcoat-perl 0.1-0.6-1 Failed 49.606992s UNKNOWN #470269: libcoat-perl: FTBFS: Failed 1/18 test scripts, 94.44% okay. 0/113 subtests failed, 100.00% okay.
libconfig-apacheformat-perl 1.2-3 Failed 43.899215s UNKNOWN #470264: libconfig-apacheformat-perl: FTBFS: Failed 12/12 test scripts, 0.00% okay. 336/336 subtests failed, 0.00% okay.
libconvert-pem-perl 0.07-2 Failed 47.513362s UNKNOWN #470322: libconvert-pem-perl: FTBFS: Failed 1/4 test scripts, 75.00% okay. 1/34 subtests failed, 97.06% okay.
libdata-float-perl 0.007-1 Failed 41.368434s UNKNOWN #470325: libdata-float-perl: FTBFS: Can't locate Module/Build.pm in @INC
libdatetime-set-perl 0.25-2 Failed 82.772836s NO_SUCH_FILE #470270: libdatetime-set-perl: FTBFS: Failed 19/19 test scripts, 0.00% okay. 9/9 subtests failed, 0.00% okay.
libdbix-class-perl 0.08009-1 Failed 239.923961s UNKNOWN NNN
libezmorph-java 1.0.4-1 Failed 159.766904s UNKNOWN #470331: libezmorph-java: FTBFS: The import junit cannot be resolved
libfile-sharedir-perl 0.05-1 Failed 43.05099s UNKNOWN #460383: libfile-sharedir-perl: FTBFS: t/02_main.......Can't locate Class/Inspector.pm in @INC  RECHECK RECHECK_GUESS
libfoundation1.0 1.0.84-2 Failed 67.279703s NO_SUCH_FILE #447464: libfoundation1.0: FTBFS: mkdir: cannot create directory `/./GNU': Permission denied  RECHECK RECHECK_TIME(67/29)
libgcr410 2.4.0-8 Failed 51.005449s GCC_ERROR #420046: libgcr410: FTBFS: ./ifdhandler.h:115: error: expected '=', ',', ';', 'asm' or '__attribute__' before 'IFDHCreateChannel'  RECHECK RECHECK_TIME(51/18)
libgearman-client-async-perl 0.94-2 Failed 114.899769s NO_SUCH_FILE #456864: libgearman-client-async-perl: FTBFS: Timeout waiting for port 9000 to startup at /build/user/libgearman-client-async-perl-0.94/t/lib/testlib.pl line 75.  RECHECK
libgettext-ruby 1.90.0-1 Failed 64.141619s UNKNOWN #470268: libgettext-ruby: FTBFS: iconv.rb:102: invalid multibyte char (SyntaxError)
libglade 1:0.17-9 Failed 21.704717s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(21/5)
libgstreamer-perl 0.09-1 Failed 110.699471s UNKNOWN #422822: libgstreamer-perl: FTBFS: Failed 3/31 test scripts, 34/492 subtests failed  RECHECK RECHECK_TIME(110/48)
libjson-ruby 1.1.1-1 Failed 112.30297s UNKNOWN #465673: libjson-ruby: FTBFS: /build/user/libjson-ruby-1.1.1/ext/json/ext/generator/extconf.rb:1:i n `require': no such file to load -- mkmf (LoadError)
libmail-box-perl 2.080-1 Failed 80.28323s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(80/18)
libmodule-runtime-perl 0.005-1 Failed 48.057297s UNKNOWN #470321: libmodule-runtime-perl: FTBFS: Can't locate Module/Build.pm in @INC
libnet-socks-perl 0.03-8 Failed 48.690883s UNKNOWN #470346: libnet-socks-perl: FTBFS: make: *** [clean] Error 1
libperldoc-search-perl 0.01-1 Failed 62.113202s UNKNOWN #422609: FTBFS: Undefined subroutine ..  RECHECK RECHECK_TIME(62/18)
libpod-constants-perl 0.16-1 Failed 72.254635s NO_SUCH_FILE caused by sbuild #395271  RECHECK
libqt4-ruby 1.4.9-8 Failed 266.664227s GCC_ERROR #470273: libqt4-ruby: FTBFS: x_7.cpp:1811: error: 'QMacCompatGLenum' was not declared in this scope
libterralib 3.0.3b2-3.1 Failed 237.50801s DPKGSHLIB #453789: libterralib: FTBFS: dpkg-shlibdeps: failure: couldn't find library libtiff.so.1 needed by debian/libterralib1c2a/usr/lib/libterralib.so.1.0.0 (its RPATH is '').  RECHECK RECHECK_TIME(237/147)
libtest-base-perl 0.47-1 Failed 49.503751s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(49/14)
libtest-exception-perl 0.25-1 Failed 59.160344s UNKNOWN #466624 RECHECK RECHECK_TIME(59/16)
libtext-quoted-perl 2.05-1 Failed 53.818048s UNKNOWN #470274: libtext-quoted-perl: FTBFS: Failed 7/7 test scripts, 0.00% okay. 11/11 subtests failed, 0.00% okay.
libtritonus-java 20070428-5 Failed 190.776751s GCC_ERROR #470330: libtritonus-java: FTBFS: constants_check.h:73: error: 'SND_SEQ_EVENT_SAMPLE' undeclared
libvisual 0.4.0-2 Failed 79.527449s GCC_ERROR #456856: libvisual: FTBFS: error: 'sigill_handler_sse' undeclared (first use in this function)  RECHECK RECHECK_TIME(79/38)
libwww-perl 5.808-1 Failed 20.783859s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(20/4)
linux-kernel-di-i386-2.6 1.57 Failed 22.319163s APT_GET_FAILED #460353: linux-kernel-di-i386-2.6: FTBFS: unmet b-dep linux-image-2.6.22-3-486  RECHECK RECHECK_TIME(22/6)
linux-modules-di-i386-2.6 1.08 Failed 23.611611s APT_GET_FAILED #460382: linux-modules-di-i386-2.6: FTBFS: unmet b-dep loop-aes-modules-2.6.22-3-486  RECHECK RECHECK_TIME(23/6)
lprng-doc 3.8.A~rc2-1 Failed 194.738634s UNKNOWN #464175  RECHECK RECHECK_GUESS RECHECK_TIME(194/133)
ltp 20060918-2 Failed 72.540281s GCC_ERROR #455429: OPEN_MAX macro removed from linux/limits.h on amd64  RECHECK RECHECK_TIME(72/36)
luarocks 0.4.3-1 Failed 57.578437s NO_SUCH_FILE #470267: luarocks: FTBFS: make[1]: *** No rule to make target `config.unix'.  Stop.
m68k-vme-tftplilo 1.1.3-1 Failed 23.631565s APT_GET_FAILED not for us.  RECHECK RECHECK_TIME(23/6)
mailscanner 4.66.5-1 Failed 69.728874s NO_SUCH_FILE #464317: mailscanner: FTBFS: chmod: cannot operate on dangling symlink `debian/mailscanner/usr/share/MailScanner/MailScanner/CustomConfig.p m'  RECHECK RECHECK_TIME(69/24)
manpages-fr-extra 20071229 Failed 80.975729s UNKNOWN #470349: manpages-fr-extra: FTBFS: po4a fails
mcvs 1.0.13-17 Failed 123.699s NO_SUCH_FILE/GCC_ERROR #450441: mcvs: FTBFS: error: 'tcflag_t' undeclared (first use in this function)  RECHECK RECHECK_TIME(123/45)
mercury 0.11.0.rotd.20040511-5 Failed 82.302362s NO_SUCH_FILE/GCC_ERROR #381813: mercury: FTBFS: cannot stat `./debian/tmp/usr/lib/mercury/bin': No such file or directory  RECHECK
migrationtools 47-3 Failed 57.214678s UNKNOWN #464294: migrationtools: FTBFS: chmod: cannot operate on dangling symlink `debian/migrationtools/usr/share/migrationtools/migrate_common.ph'  RECHECK RECHECK_TIME(57/17)
mime-tools 5.425-2 Failed 56.1559s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(56/15)
mod-bt 0.0.19+p4.2340-1 Failed 26.550412s APT_GET_FAILED #461198: mod-bt: FTBFS: unsatisfiable b-deps  RECHECK RECHECK_TIME(26/5)
modxslt 2005072700-2.2 Failed 30.202694s APT_GET_FAILED #429137: please update/request removal of your package  RECHECK RECHECK_TIME(30/5)
mono-tools 1.2.6-2 Failed 163.310852s COMPIL_FAILED #470344: mono-tools: FTBFS: ./main.cs(765,22): error CS0540: `Mono.NUnit.GUI.NUnitGUI.EventListener.SuiteFinished(TestSuiteResult )': containing type does not implement interface `NUnit.Core.EventListener'
ndisc6 0.9.5-1 Failed 55.102676s GCC_ERROR #464261: ndisc6: FTBFS: netlink.c:46: error: redefinition of 'struct nduseroptmsg'  RECHECK RECHECK_TIME(55/21)
nemerle 0.9.3+dfsg-1 Failed 99.431418s NO_SUCH_FILE #458708: nemerle: FTBFS: internal compiler error: got some unknown exception of type System.Exception: field not found  RECHECK RECHECK_TIME(99/38)
nepenthes 0.2.0-2 Failed 104.404319s UNKNOWN #369030: FTBFS with GCC 4.2: deprecated conversion from string constant...  RECHECK RECHECK_TIME(104/43)
netsurf 1.1-2 Failed 218.519318s LD_ERROR/NO_SUCH_FILE #470248: netsurf: FTBFS: parser.c:637: undefined reference to `assert'
octave2.9 1:2.9.19-2 Failed 20.155558s APT_GET_FAILED superseded anyway
octplot 0.4.0-6 Failed 134.300843s UNKNOWN #470327: octplot: FTBFS: Makefile:124: *** missing separator.  Stop.
ogle 0.9.2-5 Failed 183.482478s UNKNOWN #453220: ogle: FTBFS: dpkg-gencontrol: warning: can't parse dependency libasound2 (>> 1.0.14) libsm6  RECHECK RECHECK_TIME(183/104)
openbios-sparc 1.0~alpha2+20080106-2 Failed 57.603008s UNKNOWN Must be built on SPARC  RECHECK RECHECK_GUESS RECHECK_TIME(57/25)
palo 1.16 Failed 54.542273s GCC_ERROR #464262: palo: FTBFS: /usr/include/linux/elf.h:396: error: expected declaration specifiers or '...' before 'loff_t'  RECHECK RECHECK_TIME(54/16)
pcp 2.7.4-20080306 Failed 99.714341s NO_SUCH_FILE #470326: pcp: FTBFS: checking for ps style... unknown
php4-apd 0.4p2-6 Failed 23.034758s APT_GET_FAILED #450467: php4-apd: FTBFS: E: Package php4-dev has no installation candidate  RECHECK RECHECK_TIME(23/6)
php4-kadm5 0.2.4-4.1 Failed 23.032817s APT_GET_FAILED #418315: Don't build a php4-specific package because of php4's removal  RECHECK RECHECK_TIME(23/5)
pixie 2.2.1-1 Failed 609.856072s NO_SUCH_FILE #434669: FTBFS on hppa: "mv: cannot stat `/build/buildd/pixie-2.2.1/debian/pixie/usr/bin/show': No such file or directory"  RECHECK
postgresql-plproxy 2.0.2-1 Failed 67.394214s NO_SUCH_FILE #460356: postgresql-plproxy: FTBFS: Makefile:32: /usr/lib/pgxs/src/makefiles/pgxs.mk: No such file or directory  RECHECK RECHECK_TIME(67/18)
postgresql-tablelog 0.4.4-1 Failed 71.262121s NO_SUCH_FILE #460363: postgresql-tablelog: FTBFS: Makefile:7: /usr/lib/pgxs/src/makefiles/pgxs.mk: No such file or directory  RECHECK RECHECK_TIME(71/17)
powershell 0.9-8 Failed 21.846957s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(21/5)
pppconfig 2.3.17 Failed 56.738077s UNKNOWN #470323: pppconfig: FTBFS: po4a fails
projectcenter.app 0.4.3-3 Failed 95.920982s NO_SUCH_FILE #451460: projectcenter.app: FTBFS: Could not find /usr/lib/GNUstep/System/Makefiles/GNUstep.sh  RECHECK RECHECK_TIME(95/27)
ptop 3.6.1~b3-1 Failed 83.581965s NO_SUCH_FILE #465041: ptop: FTBFS: rm: cannot remove `ptop.1': No such file or directory  RECHECK
python-axiom 0.5.27-1 Failed 252.842922s PYTHON_TRACEBACK #470337: python-axiom: FTBFS: failed tests
qtdmm 0.8.12-2 Failed 136.871906s NO_SUCH_FILE #467604: qtdmm: FTBFS: make: *** No rule to make target `/usr/share/quilt/quilt.make'. Stop. RECHECK RECHECK_TIME(136/42)
qt-x11-free 3:3.3.8b-4 Failed 407.974545s GCC_ERROR jcristau
quagga 0.99.9-5 Failed 172.216242s UNKNOWN #469891: quagga - FTBFS: configure: error: C compiler cannot create executables
radiusclient 0.3.2-11 Failed 69.668416s DPKGSHLIB #456870: radiusclient: FTBFS: dpkg-shlibdeps: failure: couldn't find library libradiusclient.so.0 needed by debian/tmp/usr/sbin/radstatus (its RPATH is '').  RECHECK RECHECK_TIME(69/25)
rails 2.0.2-1 Failed 65.260222s UNKNOWN #464285: rails: FTBFS: private method `cp' called for File:Class  RECHECK RECHECK_TIME(65/22)
rocklight 0.1-2 Failed 23.239892s APT_GET_FAILED to be removed
saods9 4.0b7-1.5 Failed 448.589326s UNKNOWN #450475: saods9: FTBFS: mktclapp: can't open "/usr/lib/tcl8.4/history.tcl" for reading  RECHECK RECHECK_TIME(448/322)
sbaz 1.20-2 Failed 502.686974s GCC_ERROR #464259: sbaz: FTBFS: /build/user/sbaz-1.20/src/sbaz/PackageSpec.scala:35: error: unreachable code  RECHECK RECHECK_TIME(502/334)
sbcl 1:1.0.14.0-1 Unknown 0s  same bug as the one against cl-asdf.
scala 2.6.1-1lex Failed 6658.806862s UNKNOWN #443040: scala: FTBFS: GC Warning: Out of Memory! Returning NIL!  RECHECK RECHECK_TIME(6658/5170)
scalapack 1.8.0-1 Failed 22.891006s APT_GET_FAILED #462510: scalapack: FTBFS: psblas1tst.f:(.text+0x156): undefined reference to `blacs_pinfo__'  RECHECK
scli 0.3.1-3 Failed 99.625558s LD_ERROR #465632: scli: FTBFS: Nonexistent build-dependency: libsmi2  RECHECK RECHECK_VERSION RECHECK_GUESS RECHECK_TIME(99/5)
sfs 1:0.8-0+pre20060720.1-1.1 Failed 93.527974s NO_SUCH_FILE #456846: sfs: FTBFS: configure: error: Could not find NFS mount argument structure!  RECHECK RECHECK_TIME(93/36)
siege 2.66-1 Failed 73.264466s UNKNOWN #458266: siege: FTBFS: missing: line 46: aclocal-1.9: command not found  RECHECK RECHECK_TIME(73/27)
silky 0.5.4-0.1 Failed 22.320072s APT_GET_FAILED #433820: Does not build with newer libsilc  RECHECK RECHECK_TIME(22/5)
slash 2.2.6-8 Failed 21.039202s APT_GET_FAILED #429071: please update/request removal of your package  RECHECK RECHECK_TIME(21/5)
slmon 0.5.13-2.1 Failed 23.462737s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/5)
smail 3.2.0.115-7.1 Failed 73.466199s NO_SUCH_FILE #441484: smail - FTBFS: *** No rule to make target `<command-line>', needed by `dummy.o'. Stop.  RECHECK RECHECK_TIME(73/25)
snac 0.3-5 Failed 23.032229s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/5)
soundtracker 0.6.8-2 Failed 23.162287s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(23/8)
specimen 0.5.2rc3-1 Failed 140.170753s UNKNOWN #459752: specimen - FTBFS: missing build deps, libgnomecanvas, Phat 0.4.0  RECHECK RECHECK_TIME(140/53)
spplus 1.0-10 Failed 26.556269s APT_GET_FAILED #432425: spplus: FTBFS: unmet build dep php4-dev  RECHECK RECHECK_TIME(26/5)
stage 2.0.3-2 Failed 215.680796s NO_SUCH_FILE #456827: stage: FTBFS: libtool: link: `/usr/lib/libGL.la' is not a valid libtool archive  RECHECK RECHECK_TIME(215/104)
star 1.5a67-1.2 Failed 23.298839s APT_GET_FAILED #470342: star: FTBFS: Nonexistent build-dependency: smake
stlport4.6 4.6.2-3 Failed 196.32288s GCC_ERROR #434691: stlport4.6: FTBFS with g++-4.2  RECHECK
subcommander 1.2.2-1 Failed 20.904866s APT_GET_FAILED #461196: subcommander: FTBFS: unsatisfiable build-deps  RECHECK RECHECK_TIME(20/7)
svk 2.0.1-1 Failed 78.468283s UNKNOWN caused by sbuild #395271  RECHECK RECHECK_TIME(78/21)
swh-plugins 0.4.15-0.1 Failed 63.563443s UNKNOWN #445795: swh-plugins: FTBFS: unmet b-dep fftw3-dev  RECHECK RECHECK_TIME(63/16)
syncekonnector 0.3.2-3 Failed 258.67566s UNKNOWN #427308: syncekonnector: FTBFS: configure: error: Can't find libkonnector.so  RECHECK RECHECK_TIME(258/107)
synce-multisync-plugin 0.9.0-4 Failed 205.43555s UNKNOWN #470348: synce-multisync-plugin: FTBFS: Unmet build dependencies: librra0-dev (>= 0.9.1)
systemimager 3.6.3dfsg1-3 Failed 333.815525s HEADER_NO_SUCH_FILE/NO_SUCH_FILE/GCC_ERROR #422825: systemimager: FTBFS: tr.c:34: error: expected specifier-qualifier-list before '__u16'  RECHECK
tagcolledit 1.3-1 Failed 20.166172s APT_GET_FAILED #432437: tagcolledit: FTBFS: unmet build dep libtagcoll-dev  RECHECK RECHECK_TIME(20/6)
taoframework 2.0.0.svn20071027-3 Failed 259.706379s COMPIL_FAILED #456857: taoframework: FTBFS: error CS0006: cannot find metadata file `/usr/lib/cli/nunit-2.2.6/nunit.framework.dll'  RECHECK RECHECK_GUESS RECHECK_TIME(259/176)
tcm 2.20+TSQD-4 Failed 90.54182s UNKNOWN #464288: tcm: FTBFS: cp: not writing through dangling symlink `src/Config.tmpl'  RECHECK RECHECK_TIME(90/38)
telegnome 0.0.10-7 Failed 22.319312s APT_GET_FAILED #464331: telegnome: FTBFS: E: Package libgdk-pixbuf-gnome-dev has no installation candidate  RECHECK RECHECK_TIME(22/5)
thaifonts-scalable 1:0.4.9-1 Unknown 0s  TODO
timps 0.25-1 Failed 69.060015s GCC_ERROR #470247: timps: FTBFS: conn.c:1118: error: too few arguments to function 'nbio_sfd_newlistener'
tkmixer 1.0-15 Failed 48.145068s LD_ERROR #434016: tkmixer: FTBFS: /usr/bin/ld: unknown architecture of input file `mix_init.o' is incompatible with i386 output  RECHECK RECHECK_TIME(48/14)
tomboy 0.8.2-1 Failed 295.658188s COMPIL_FAILED #470333: tomboy: FTBFS: PreferencesDialog.cs(5,1): error CS0246: The type or namespace name `GConf.PropertyEditors' could not be found.
ttf-jsmath 0.01-1 Failed 50.292711s NO_SUCH_FILE #470343: ttf-jsmath: FTBFS: make: dh_installdefoma: Command not found
uclibc 0.9.27-1 Failed 55.975749s NO_SUCH_FILE #336367: FTBFS: uclibc missing asm-i486/mman.h  RECHECK RECHECK_TIME(55/25)
uswsusp 0.7-1 Failed 102.009396s GCC_ERROR #456841: uswsusp: FTBFS: /usr/include/splashy.h:67: error: expected ')' before 'visible'  RECHECK RECHECK_TIME(102/40)
vat 4.0b2-15.2 Failed 78.277668s UNKNOWN #450476: vat: FTBFS: can't find tcl/init.tcl  RECHECK RECHECK_TIME(78/26)
vcs-tree 0.3.1-1 Unknown 0s filed against cl-asdf
vdkbuilder2 2.4.0-4.1 Failed 146.699525s HEADER_NO_SUCH_FILE/NO_SUCH_FILE/GCC_ERROR XBASE
vdkxdb2 2.4.0-3 Failed 139.080387s HEADER_NO_SUCH_FILE/NO_SUCH_FILE/GCC_ERROR XBASE
wlassistant 0.5.7-2 Failed 273.12807s PYTHON_TRACEBACK #456834: wlassistant: FTBFS: TypeError: coercing to Unicode: need string or buffer, list found  RECHECK RECHECK_TIME(273/102)
wmii2-doc 1-5 Failed 122.070097s NO_SUCH_FILE #458720: wmii2-doc: FTBFS: LaTeX Error: File `kvoptions.sty' not found.  RECHECK RECHECK_TIME(122/33)
wmii-doc 1:1-7 Failed 123.443287s NO_SUCH_FILE #458715: wmii-doc: FTBFS: LaTeX Error: File `kvoptions.sty' not found.  RECHECK RECHECK_TIME(123/36)
wmusic 1.5.0-3 Failed 22.644014s APT_GET_FAILED #456743: wmusic: Please use an XMMS alternative.
wsjt 5.9.7.r383-1 Failed 21.993093s APT_GET_FAILED #470339: wsjt: FTBFS: Nonexistent build-dependency: libgfortran1
xbsql 0.11-6 Failed 64.203586s HEADER_NO_SUCH_FILE/NO_SUCH_FILE/GCC_ERROR XBASE
xen-unstable 3.0-unstable+hg11561-1 Failed 21.291109s APT_GET_FAILED #399700: FTBFS, build-deps on linux-support-2.6.17-2  RECHECK RECHECK_TIME(21/7)
xindy 2.3-1 Failed 222.51284s GCC_ERROR #467585: xindy: FTBFS: ordrulei.c:24: error: 'clisp_dirent_off_t' undeclared (first use in this function) RECHECK RECHECK_TIME(222/108)
xine-plugin 1.0.1~cvs20070523-1 Failed 85.922195s UNKNOWN #464321: xine-plugin: FTBFS: checking for XINE-LIB version >= 1.0.0... /usr/bin/xine-config: line 68: exec: pkg-config: not found  RECHECK RECHECK_TIME(85/28)
xmovie 1.9.12-1.1 Failed 122.512309s TOO_MANY_ARGS/GCC_ERROR #436173: xmovie_1.9.12-1.1 (i386/unstable): FTBFS: 'BC_TRANSPARENCY' was not declared in this scope  RECHECK RECHECK_TIME(122/33)
xpuyopuyo 0.9.8-3 Failed 22.197626s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(22/7)
xserver-xorg-video-intel 2:2.2.1-1 Failed 128.745027s NO_SUCH_FILE #470266: pciaccess check should use >= 0.10 instead of 0.10.0
xsp 1.2.5-2 Failed 102.52239s COMPIL_FAILED #458709: xsp: FTBFS: error CS0006: cannot find metadata file `System.Web.dll'  RECHECK RECHECK_TIME(102/38)
xtla 1.2.1-1 Failed 20.372074s APT_GET_FAILED #403058: xtla: FTBFS: b-dep on emacs-snapshot, which is not in testing  RECHECK RECHECK_TIME(20/6)
xwine 1.0.1-1.1 Failed 20.926136s APT_GET_FAILED GNOME  RECHECK RECHECK_TIME(20/7)

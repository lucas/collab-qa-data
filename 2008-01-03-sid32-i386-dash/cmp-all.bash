#!/bin/bash

for i in $(cd sid32 ; ls); do
  debdiff sid32/$i/*changes sid32-dash/$i/*changes > $i.debdiff
done

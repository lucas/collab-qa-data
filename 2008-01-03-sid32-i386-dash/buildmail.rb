#!/usr/bin/ruby -w

Dir::glob('*.debdiff').each do |file|
pkg = file.gsub(/\.debdiff$/,'')
puts pkg
bh = IO::read(Dir::glob("logs/bash/#{pkg}_*log")[0]).split(/\n/).grep(/DC-Build-Header/)
ver = bh[0].split(' ')[2]
debdiff = IO::read(file)
f = File::new("#{pkg}.mail", 'w')
f.puts <<-EOF
To: submit@bugs.debian.org
Subject: #{pkg}: package content changes when built with dash as /bin/sh

Package: #{pkg}
Version: #{ver}
Severity: important
User: debian-qa@lists.debian.org
Usertags: qa-ftbfs-dash-2008-01-03 qa-ftbfs-dash

Hi,

I rebuilt all packages in Debian, first with bash as /bin/sh, then with dash as
/bin/sh. Your package builds fine in both cases. However, the resulting
packages are diferent according to debdiff!

The logs for both builds can be found at:
http://people.debian.org/~lucas/logs/2008/01/03.dash-different/

Here is the debdiff output ("First" = bash, "Second" = dash):
#{debdiff}

Thank you,
EOF
end

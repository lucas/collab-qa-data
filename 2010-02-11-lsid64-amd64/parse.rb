#!/usr/bin/ruby -w

require 'pp'

# (?:  => non-capturing group
# pp IO::read('edos').scan(/^\S.*\n(?:\s+.*\n)*/)
# (?=  => using forward-lookahead
# pp IO::read('edos').split(/^(?=[^ ])/)
pkgs = IO::readlines('failed.2010-02-11.txt').grep(/BUILDDEPS.*TODO/).map { |l| l.split(' ')[0..1] }
IO::read('edos').split(/^(?=[^ ])/).map { |l| l.chomp }.each do |b|
  err = b.split(/\n/)
  next if err.include?("  libjpeg8-dev (= 8-2) and libjpeg62-dev (= 6b-16) conflict")
  pv = [ err[0].split(' ')[0], err[0].split(' ')[2].chop.chop]
  next if not pkgs.include?(pv)
  next if b =~ /ghc6/ or b =~ /haskell/ or b =~ /hdbc-/
  next if err.length > 8
  puts "#{b}"
end
